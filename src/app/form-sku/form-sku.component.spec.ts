import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSkuComponent } from './form-sku.component';
import { ReactiveFormsModule, FormsModule, Validators } from '@angular/forms';
import { By } from '@angular/platform-browser';  // Para informe de selectores, llegar a la propiedad  html y verificar si se está renderizando o no una variable de ese componente
import { DebugElement } from '@angular/core';// Entrar a mirar el elemento como tal en html


describe('FormSkuComponent', () => {
  let component: FormSkuComponent;
  let fixture: ComponentFixture<FormSkuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormSkuComponent ],
      imports: [
        ReactiveFormsModule, 
        FormsModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSkuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('skuForm should be created', () => {
    expect(component.skuForm).toBeTruthy();
  });

  it('skuField should be created', () => {
    expect(component.skuField).toBeTruthy();
  });

  it('skuName should be created', () => {
    expect(component.skuNameField).toBeTruthy();
  });

  describe('Test for skuField', () => {

/*     it('Should not have an error: required', () => {
      component.skuField.setValue('sfgsdffwrt123');
      expect(component.skuField.valid).toBeTruthy();
    }) */ // Sin el custom validador

    it('Should not have an error: invalidSku', () => {
      component.skuField.setValue('12312341234');
      expect(component.skuField.valid).toBeTruthy();
    })

    it('Should have an error: invalidSku', () => {
      component.skuField.setValue('sfgsdffwrt123');
      expect(component.skuField.invalid).toBeTruthy();
      expect(component.skuField.getError('invalidSku')).toBeTruthy();
    })

    it('Should have an error: required', () => {
      component.skuField.setValue('');
      expect(component.skuField.invalid).toBeTruthy();
      expect(component.skuField.getError('required')).toBeTruthy();
    })

    it('Should show error in template: invalidSku', async(() => {
      // Arrange
      let input = fixture.debugElement.query(By.css('input#skuInput')).nativeElement;
      // Act
      input.value = "987654";
      input.dispatchEvent(new Event('input')); // NOtifica emulación de evento input
      fixture.detectChanges();
      fixture.whenStable() // Después de haber realizado los cambios
        .then(() => {
           // Assert
           let msgs: any[] = fixture.nativeElement.querySelectorAll('.ui.message'); // querySelector devuelve un array
           console.log(msgs);
           expect(msgs.length).toEqual(1);
           expect(msgs[0].innerHTML).toContain("SKU is invalid");

        })
    }))

    it('Should show error in template: required', async(() => {
      // Arrange
      let input = fixture.debugElement.query(By.css('input#skuInput')).nativeElement;
      // Act
      input.value = "";
      input.dispatchEvent(new Event('input')); // NOtifica emulación de evento input
      fixture.detectChanges();
      fixture.whenStable() // Después de haber realizado los cambios
        .then(() => {
           // Assert
           let msgs: any[] = fixture.nativeElement.querySelectorAll('.ui.message'); // querySelector devuelve un array
           console.log(msgs);
           expect(msgs.length).toEqual(1);
           expect(msgs[0].innerHTML).toContain("SKU is required");

        })
    }))


  })

});
